import React, { Component } from 'react';
import './css/home.css';
import AccountOverview from './AccountOverview';
import OurOffers from './OurOffers';
import LeftSideMenu from './LeftSideMenu';
import RecentTransactions from './RecentTransactions';
import FrequentTransactions from './FrequentTransactions';


export default class MainContainer extends Component {



    render() {
        return (

            <div className="mainContainer">
                <div class="row" >
                    <div class="col-sm" >
                        <LeftSideMenu />
                    </div>
                    <div class="col-sm-6" >
                        <AccountOverview />
                    </div>


                    <div class="col-sm" >
                        <OurOffers />
                    </div>
                </div>
                <br />


                <div className="row" style={{padding: "20px"}} >

                    <RecentTransactions />
                </div>

<div className="row" style={{padding: "20px"}} >

                    <FrequentTransactions />
                </div>
                <div className="row">
    <img width="100%" src={require('../assets/footer.PNG')}/>
    </div>
            </div>

        );
    }
}
