import React,{Component} from 'react';
import './css/logincomponent.css';
import Header1 from './Header1';
import Header2 from './Header2';
import LoginModal from './LoginModal';
export default class LoginComponents extends Component {

state = {
        isModalVisible: false
    }

    signIn () {
        this.setState({
            isModalVisible: true
        })
    }

    handleClose() {
        console.log('handleClose')
        this.setState({ isModalVisible: !this.state.isModalVisible })
    }
    
    routeNextPage() {
         this.setState({ isModalVisible: !this.state.isModalVisible })
        this.props.history.push('/home');
    }

render(){
      return (
    
  
  
  <div style={{marginLeft:"-13"}}>
       <Header2  bgColor={"navbar navbar-expand-sm bg-primary navbar-dark"}/>
       <Header1  bgColor={"navbar navbar-expand-sm bg-light navbar-light"}/>
    <div class="container-fluid">

        <div class="row login">
<div class="login-box">
                <div class="row">
                    <div class="col-lg-10 login-portal">
                        <div class="login-div">
                            <p style={{marginBottom: "5px"}}>Login To Internet Banking</p>
                            <div class="form-group">
                                <span htmlFor="usr">Username</span>
                                <input type="text" class="form-control LoginText paddTop" id="usr" placeholder="Username"/>
       
                                    <span htmlFor="pwd">Password</span>
                                    <input type="password" class="form-control LoginText paddTop" id="pwd" placeholder="Password"/>
      
                                    <div class="dropdown">
                                        <span htmlFor="pwd">Start In</span>
                                            <button type="button" class="btn btn-primary dropdown-toggle LoginText paddTop" data-toggle="dropdown">
                                                My Account  <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">My Account</a></li>
                                                <li><a href="#">Customer Services</a></li>
                                                <li><a href="#">Investment</a></li>
                                                <li><a href="#">Insurance</a></li>
                                            </ul>
       </div>

                                        <div class="form-check paddTop">
                                            <span class="form-check-span">
                                                <input type="checkbox" class="form-check-input" value=""/>Remember se
         </span>
       </div>

                                       <button type="button" class="btn btn-primary btn-block signIn LoginBtnText"onClick={() => this.signIn()}>Sign In</button>
                                        <a href="#" class="forget paddTop">Forget Username/Password ?</a>
                                      <hr style={{background:"white",margin: "10px"}}/>
                                            <a href="#" class="forget" style={{paddingLeft: "20%",marginTop:"5px"}} >Register for online services</a>
                                             </div>

       </div>

                                    </div>
                                </div>
                            </div>

                        </div>

    
                   </div>

<div class="row bottomleft">
   <p className="footer1" style={{paddingLeft:"50px"}}> Choose what’s right for you!</p>
</div>
<div class="row" style={{background: "#F4F4F4",paddingLeft:"35px",margin:"0px",marginBottom:"150px"}}>
<div class="col-sm-2 borderCorner" style={{position: "absolute",bottom:"50px",width:"150px",left:"150px"}}>
                <img  className="boxImgText" src={require('../assets/icon01.png')}/><br />
               <p className="boxText">SAVING ACCOUNT</p></div>
<div class="col-sm-2 borderCorner"  style={{position: "absolute",bottom:"50px",width:"150px",left:"310px"}}>
                <img className="boxImgText" src={require('../assets/icon02.png')}/><br />
               <p className="boxText">INVESTMENTS</p></div>

<div class="col-sm-2 borderCorner"  style={{position: "absolute",bottom:"50px",width:"150px",left:"470px"}}>
                   <img  className="boxImgText" src={require('../assets/icon03.png')}/><br />
               <p className="boxText">HOME LENDING</p></div>

<div class="col-sm borderCorner" s style={{position: "absolute",bottom:"50px",width:"150px",left:"630px"}} >
                <img className="boxImgText" src={require('../assets/icon04.png')}/><br />
               <p className="boxText">CAR BUYING & LOANS</p></div>

<div class="col-sm borderCorner"  style={{position: "absolute",bottom:"50px",width:"150px",left:"790px"}}>
                <img className="boxImgText" src={require('../assets/icon05.png')}/><br />
               <p className="boxText">PERSONAL CARDS</p></div>

               <div class="col-sm borderCorner" style={{position: "absolute",bottom:"50px",width:"150px",left:"950px"}}>
                <img className="boxImgText" src={require('../assets/icon06.png')}/><br />
               <p className="boxText">CORPORATE CARDS</p></div>
   </div>




<div class="row" style={{background: "white",font: "Regular 18px Segoe UI",paddingTop:"30px"}}>
   <p className="footer1" style={{color: "#08273D",paddingLeft:"50px"}}>Tips For Your Financial Life</p>
</div>
<div class="row boxText2" style={{background: "white",paddingLeft:"15px"}}>
<div class="col-sm-3" style={{background: "white",margin: "0px"}}>
                <img src={require('../assets/tips_01.png')}  width="220" height="120"/>
                 <p style={{fontSize:"12px"}}>Make saving your habit</p>
                  <p >Take inventory of saving opportunities. Consider opportunities cost of each of your expenses and potential purchases</p>
                 </div>
<div class="col-sm-3"  style={{background: "white",margin: "0px"}}>
                <img src={require('../assets/tips_02.png')} width="220" height="120"/>
                                                                            <p style={{fontSize:"12px"}}>Make saving your habit</p>
                                                                            <p>Take inventory of saving opportunities. Consider opportunities cost of each of your expenses and potential purchases</p>
    </div>

<div class="col-sm-3" style={{background: "white",margin: "0px"}}>
              <img src={require('../assets/tips_03.png')} width="220" height="120"/>
                                                                                <p style={{fontSize:"12px"}}>Make saving your habit</p>
                                                                                <p>Take inventory of saving opportunities. Consider opportunities cost of each of your expenses and potential purchases</p>
    </div>

<div class="col-sm-3" style={{background: "white",margin: "0px"}}>
               <img src={require('../assets/tips_04.png')} width="220" height="120"/>
                                                                                    <p style={{fontSize:"12px"}}>Make saving your habit</p>
                                                                                    <p>Take inventory of saving opportunities. Consider opportunities cost of each of your expenses and potential purchases</p>
    </div>
<div className="row">
    <img width="100%" src={require('../assets/footer.PNG')}/>
    </div>
   </div>

 <LoginModal show={this.state.isModalVisible} handleClose={() => this.handleClose()} routeNextPage={() => this.routeNextPage()}/>
</div>

  
  );
}
}

