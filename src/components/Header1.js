
import React, { Component } from 'react';

export default class Header1 extends Component {

render(){
return(
 <div class="col-lg header1" >
  <nav class={!this.props.bgColor? "navbar navbar-expand-sm bg-primary navbar-dark":this.props.bgColor} style={{height:"40px",marginLeft:this.props.bgColor && "-13px",marginRight:this.props.bgColor && "-13px"}}>
    <div class="collapse navbar-collapse" id="navb">
    <ul class="navbar-nav mr-auto">
       <img src={require('../assets/logo.svg')} style={{height: "40px"}}/>
      <li class="nav-item">
        <a class="nav-link" href="javascript:void(0)">Banking</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="javascript:void(0)">Borrowing</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="javascript:void(0)">Investment</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="javascript:void(0)">Insurance</a>
      </li>
    </ul>
    {!this.props.bgColor &&
    <form class="form-inline my-2 my-lg-0">
        <div class="row" style={{display:"block"}}>
       <img src={require('../assets/user.svg')} style={{margin:"15px",width:"15px"}} />
      <img src={require('../assets/alarm-bell.svg')} style={{width:"15px" ,marginRight:"100px"}}/>
      </div>
    </form>
    }
  </div>
</nav>
</div>


)
}
}
